use crate::lavacache::LavaCache;
use anyhow::{Context, Result};

use prometheus::{opts, register_gauge_vec, Encoder, GaugeVec, TextEncoder};
use std::sync::Arc;
use warp::Filter;

#[derive(Clone)]
pub struct Metrics {
    lava: Arc<LavaCache>,
    devices: GaugeVec,
    tags: GaugeVec,
    workers: GaugeVec,
}

impl warp::reject::Reject for AnyhowRejection {}
#[derive(Debug)]
struct AnyhowRejection {
    error: anyhow::Error,
}

impl Metrics {
    pub fn new(lava: Arc<LavaCache>) -> Result<Metrics> {
        let opts = opts!("lava_device", "Status of Lava Devices Under Test");
        let devices = register_gauge_vec!(opts, &["health", "device", "device_type"])
            .context("Failed to register board gauges")?;

        let opts = opts!("lava_worker", "Lava device to worker mapping");
        let workers = register_gauge_vec!(opts, &["worker", "device", "location"])
            .context("Failed to register worker gauges")?;

        let opts = opts!("lava_tag", "Lava device to tags mapping");
        let tags = register_gauge_vec!(opts, &["tag", "device"])
            .context("Failed to register tag gauges")?;

        Ok(Metrics {
            lava,
            devices,
            tags,
            workers,
        })
    }

    async fn update_gauges(&self) -> Result<()> {
        fn worker_to_location(worker: &str) -> &str {
            let parts: Vec<&str> = worker.split('-').collect();
            if parts.len() == 4 {
                return parts[2];
            }

            if worker.contains("cbg") {
                return "cbg";
            }

            "unknown"
        }

        self.devices.reset();
        self.workers.reset();

        let devices = self.lava.devices().await?;
        for (hostname, d) in devices.iter() {
            use lava_api::device::Health::*;
            let state = match d.health {
                Unknown => "unknown",
                Maintenance => "maintenance",
                Good => "good",
                Bad => "bad",
                Looping => "looping",
                Retired => continue,
            };

            self.devices
                .with_label_values(&[state, &hostname, &d.device_type])
                .set(1f64);
            let location = worker_to_location(&d.worker);
            self.workers
                .with_label_values(&[&d.worker, &hostname, location])
                .set(1f64);
            for t in d.tags.iter() {
                self.tags.with_label_values(&[&t.name, hostname]).set(1f64);
            }
        }

        Ok(())
    }

    pub async fn listen(self) {
        let metrics = warp::path!("metrics").and_then(move || {
            let s = self.clone();

            async move {
                s.update_gauges()
                    .await
                    .map_err(|error| warp::reject::custom(AnyhowRejection { error }))?;

                let encoder = TextEncoder::new();
                let families = prometheus::gather();
                let mut buffer = Vec::new();
                encoder
                    .encode(&families, &mut buffer)
                    .expect("Encoding failed");

                if false {
                    Err(warp::reject::not_found())
                } else {
                    Ok(buffer)
                }
            }
        });

        warp::serve(metrics).run(([0, 0, 0, 0], 8884)).await;
    }
}
