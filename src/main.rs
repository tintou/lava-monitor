use anyhow::{anyhow, Context, Result};
use futures::future::OptionFuture;
use lava_api::Lava;
use mattermost_rs::Mattermost;
use serde::Deserialize;
use std::fs::File;
use std::path::PathBuf;
use std::sync::Arc;
use std::time::Duration;
use structopt::StructOpt;

pub mod lavacache;
use lavacache::LavaCache;

mod metrics;
use metrics::Metrics;

mod monitor;
use monitor::Monitor;

#[derive(Clone, Debug, Deserialize)]
struct MattermostConfig {
    hook: String,
    interval: Option<u64>,
}

#[derive(Clone, Debug, Deserialize)]
struct LavaConfig {
    url: String,
    token: Option<String>,
}

#[derive(Clone, Debug, Deserialize)]
struct Config {
    mattermost: MattermostConfig,
    lava: LavaConfig,
}

#[derive(Debug, StructOpt)]
struct Opt {
    #[structopt(short, long, parse(from_os_str))]
    config: Option<PathBuf>,
    #[structopt(short, long, env = "MATTERMOST_HOOK")]
    mm_hook: Option<String>,
    #[structopt(short = "i", long, env = "MATTERMOST_INTERVAL")]
    mm_interval: Option<u64>,
    #[structopt(short = "t", long, env = "LAVA_TOKEN")]
    lava_token: Option<String>,
    #[structopt(env = "LAVA_URL")]
    lava_url: Option<String>,
}

#[tokio::main]
async fn main() -> Result<()> {
    let env = env_logger::Env::default()
        .filter_or("LAVA_MONITOR_LOG", "lava_monitor=info")
        .write_style("LAVA_MONITOR_WRITE_STYLE");
    env_logger::init_from_env(env);

    let opt = Opt::from_args();

    let config: Option<Config> = if let Some(p) = opt.config {
        let f = File::open(p).context("Failed to open config file")?;
        Some(serde_yaml::from_reader(f).context("Failed to parse config")?)
    } else {
        None
    };

    let hook: Option<&str> = if let Some(hook) = opt.mm_hook.as_ref() {
        Some(hook)
    } else if let Some(config) = config.as_ref() {
        Some(&config.mattermost.hook)
    } else {
        println!("No mattermost hook url given; only providing metrics");
        None
    };

    let interval = if let Some(interval) = opt.mm_interval {
        Duration::from_secs(interval)
    } else if let Some(interval) = config.as_ref().and_then(|c| c.mattermost.interval) {
        Duration::from_secs(interval)
    } else {
        Duration::from_secs(300)
    };

    let token = if opt.lava_token.is_some() {
        opt.lava_token
    } else if let Some(token) = config.as_ref().and_then(|c| c.lava.token.clone()) {
        Some(token)
    } else {
        None
    };

    let url = if let Some(url) = opt.lava_url.as_ref() {
        url
    } else if let Some(url) = config.as_ref().map(|c| c.lava.url.as_str()) {
        url
    } else {
        return Err(anyhow!("No lava URL provided"));
    };

    let lava = Lava::new(&url, token)?;
    let cache = Arc::new(LavaCache::new(lava));

    let mm_job = if let Some(hook) = hook {
        let mm = Mattermost::new(hook)?;
        let cache = cache.clone();
        let mm_job = tokio::spawn(async move { Monitor::monitor(cache, mm, interval).await });
        Some(mm_job)
    } else {
        None
    };

    let metrics = Metrics::new(cache)?;
    let prometheus_job = tokio::spawn(async move {
        metrics.listen().await;
    });

    tokio::select! {
        Some(r) = OptionFuture::from(mm_job), if mm_job.is_some() =>  {
            return match r {
                Ok(_) => Err(anyhow!("Unexpected exit from Mattermost hook")),
                Err(e) => Err(e.into()),
            }
        }
        _ = prometheus_job => {
            return Err(anyhow!("Metrics exited"));
        }
    }
}
